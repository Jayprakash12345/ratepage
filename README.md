# RatePage

A MediaWiki extension that enables users to rate pages on a wiki.
More information: https://www.mediawiki.org/wiki/Extension:RatePage

Initially developed to be used on Nonsensopedia https://nonsa.pl/